## _Not Walrus_

#### _A simple web page that uses jQuery to show/hide elements, 8/9/16_

#### By _**Aimen Khakwani and Ewa Manek**_

## Description

_This is a simple HTML web page that shows and hides an image using jQuery._

## Setup/Installation Requirements

*Clone from GitHub*
*Replace with your work and information*

## Support and Contact

_Please contact me through GitHub_

## Technologies Used

_HTML, CSS, jQuery_

## License

Copyright (c) 2016 **_Aimen Khakwani and Ewa Manek_**
