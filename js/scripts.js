$(document).ready(function(){
  $(".clickable").click(function(){
    $("#initially-showing").fadeToggle("slow", "linear");
    $("#initially-hidden").fadeToggle("slow", "linear");
  });

  $("#cheetah").slideDown("fast");
});
